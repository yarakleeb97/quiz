<?php

namespace App\GraphQL\Mutations\Admin;

use App\Models\Quiz;
use App\Models\Subject;

final class QuizMutator
{
    /**
     * @param  null  $_
     * @param  array{}  $args
     */
    public function create($_, array $args)
    {
        Subject::findOrFail($args['subject_id']);
        $quiz = new Quiz($args);
        $quiz->save();
        return $quiz;
    }
}
