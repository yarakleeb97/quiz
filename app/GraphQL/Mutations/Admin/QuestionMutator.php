<?php

namespace App\GraphQL\Mutations\Admin;

use App\Models\Question;
use App\Models\Quiz;

final class QuestionMutator
{
    public function create($_, array $args)
    {
        Quiz::findOrFail($args['quiz_id']);
        $question = new Question();
        $question->title = $args["title"];
        $question->quiz_id = $args["quiz_id"];
        $question->answers = json_encode($args["answers"]);
        $question->correct_answer = json_encode($args["correct_answer"]);
        $question->type = $args["type"];
        $question->mark = $args["mark"];
        $question->save();
        return $question;
    }

    public function update($_, array $args)
    {
        $question = Question::findOrFail($args['id']);
        $question->title = $args["title"] ?? $question->title;
        $question->answers = isset($args["answers"]) ? json_encode($args["answers"]) : $question->answers;
        $question->correct_answer = isset($args["correct_answer"]) ? json_encode($args["correct_answer"]) : $question->correct_answer;
        $question->type = $args["type"] ?? $question->type;
        $question->mark = $args["mark"] ?? $question->mark;
        $question->save();
        return $question;
    }
}
